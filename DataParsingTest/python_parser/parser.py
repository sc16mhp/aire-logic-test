import json
import xml.etree.ElementTree as ET

# This is my attempt for the DataParsingTests
# The OOP approach I have taken could be optimised more
# by combining child, friend, family_member into one object person
# with parameters name, age, relationship
# this would just neaten up definitions.

class child:
    # class used to describe infomation about a child
    # note - quotes allows ou to choose if the name reads either as: "name" (true) or name (false)
    def __init__(self, name, age, quotes):
        if (quotes == True):
            self.name = '"{0}"'.format(name)
        else:
            self.name = name
        self.age = age


class address:
    # class used to describe infomation about an address
    # quotations are added as the answers require it
    def __init__(self, postcode, building_name_or_number, street, city):
        self.postcode = '"{0}"'.format(postcode)
        self.postal_area = '"{0}"'.format(postcode[0:2])
        self.building_name_or_number= '"{0}"'.format(building_name_or_number)
        self.street= '"{0}"'.format(street)
        self.city= '"{0}"'.format(city)

class dob:
    #class used to describe a date of birth
    def __init__(self, year):
        self.year = '"{0}"'.format(year)

class friend:
    # class used to describe a friend
    def __init__(self, name):
        self.name = name

class brand_preference:
    # class used to describe preferences for brands
    def __init__(self, name, type):
        self.name = name
        self.type = type

class family_member:
    # class used to describe family members
    def __init__(self, name, relationship):
        self.name = name
        self.relationship = relationship

class performance:
    #class used to hold a performance score
    def __init__(self, average):
        self.average = '"{0}"'.format(average)


class answers:
    # class used to store all the answers to be passed out
    def __init__(self, name, children, address, dob, friends, brand_preferences, family, performance):
        self.name = name
        self.children = children
        self.address = address
        self.dob = dob
        self.friends = friends
        self.brand_preferences = brand_preferences
        self.family = family
        self.performance = performance

class Parser:

    def get_name(self, data_name, data_meta):
        # parses and returns the name
        x = data_name.split(", ")
        y = x[1].split(" ")
        title = data_meta.split(", ")[0].split(": ")[1]
        title = title[1] + title[2] + " "
        name = title + y[0] + " " + y[1][0] + " " + x[0]
        return '"{0}"'.format(name)

    def get_children(self, data, homer_name, quotes):
        # parses and returns the children
        lname = homer_name.split(",")[0]
        bart_data = data[0]["data"]
        lisa_data = data[1]["data"]
        # Using ET parses the XML such that xml[0] contains
        # the name of bart or lisa
        # and xml[1] thier age.
        xml = ET.fromstring(bart_data)
        bart = child(xml[0].text + " " + lname, xml[1].text, quotes)
        xml = ET.fromstring(lisa_data)
        lisa = child(xml[0].text + " " + lname, xml[1].text, quotes)
        children = [bart, lisa]
        return children

    def get_address(self, data):
        # parses and returns the address
        # To get the address I splt the data accordingly
        # So I can reformat it correctly by concatination
        # of strings
        add1= data.split(",")
        add2 = add1[0].split(" ")
        add3 = add1[-1].split(" ")
        return address(add3[1] + add3[2], add2[0], add2[1] + " " + add2[2], add1[2][1:])

    def get_dob(self, data):
        # parses and returns the dob
        # For this need to know the relation between
        # the number -308706720 and 1960
        # can't find any link???????
        data = int(data)
        # for now do this (not correct)
        year = 308708680 + data
        return dob(year)

    def get_friends(self, data):
        # parses and returns the friends
        # Split the data up to remove spaces then pick
        # the positions with moe and ned in
        data = data.split(" ")
        # Ned is not a friends so he is not included
        friends = [friend(data[0]), friend(data[7])]
        return friends

    def get_bp(self, data):
        # parses and returns the brand preferences
        data = data.split("\n")
        pref1 = brand_preference(data[1].split(": ")[1], data[2].split(": ")[1])
        pref2 = brand_preference(data[3].split(": ")[1], data[4].split(": ")[1])
        preferences = [pref1, pref2]
        return preferences

    def get_family(self, data, children):
        # parses and returns the family
        family = []
        for f, l, r in zip(data['first_name'], data['last_name'], data['relationship']):
            family.append(family_member(f + " " + l, r))
        family.insert(1, family_member(children[0].name, "Child"))
        family.insert(2, family_member(children[1].name, "Child"))
        return family

    def get_performance(self, data):
        # parses and returns the performance score
        # this is in tabluar form aka
        # Id                                    |Date           |Safety |Punctuality    |Performance
        # 8c4010f6-0386-4d63-a2d4-491772240f77  |201029121422   |3      |0              |0
        # 3d8a4970-983b-4068-bc3b-b77e1cbe31c9  |201128121422   |1      |0              |0
        # e00357a6-02e0-4625-bf52-f63164cd9f14  |201229121422   |0      |0              |-1

        table = data.split("\n")
        user1_oneline = table[1]
        user1 = user1_oneline.split("|")
        user1_score = int(user1[2:][0]) + int(user1[2:][1]) + int(user1[2:][2])

        user2_oneline = table[2]
        user2 = user2_oneline.split("|")
        user2_score = int(user2[2:][0]) + int(user2[2:][1]) + int(user2[2:][2])

        user3_oneline = table[3]
        user3 = user3_oneline.split("|")
        user3_score = int(user3[2:][0]) + int(user3[2:][1]) + int(user3[2:][2])

        true_average = int((user1_score + user2_score + user3_score)/3) # average the scores between each user ID

        return performance(true_average)

    def parse_file(self, filename):
        with open(filename, 'r') as f:
            return self.parse(f.read())

    def parse(self, content):
        # essetily the main function for this unit

        #load the data
        jsondata = json.loads(content)

        # get theobjects to send off
        name = self.get_name(jsondata["name"], jsondata["meta"])

        children = self.get_children(jsondata["children"], jsondata["name"], True)

        addresss = self.get_address(jsondata["address"])

        dob = self.get_dob(jsondata["dateOfBirth"])

        friends = self.get_friends(jsondata["friends"])

        brand_preferences = self.get_bp(jsondata["brand_preferences"])

        family = self.get_family(jsondata["family"], self.get_children(jsondata["children"], jsondata["name"], False))

        performance = self.get_performance(jsondata["performance_review_scores"])

        #combine the objects together into one class to be passed to data
        homer = answers(name, children, addresss, dob, friends, brand_preferences, family, performance)
        return homer
